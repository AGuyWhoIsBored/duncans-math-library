# Duncan's Math Library

Small library that I created to do some math functions and algorithms!

The implemented libraries currently include:

- persistencelib
    * This library contains some functions and brute-fore algorithms to compute Persistence values of numbers.

Additionally, I have implemented a simple UI wrapper to enable users to utilize the libraries that I have and will write!

I am planning on adding more mathematical libraries in this program in the future!
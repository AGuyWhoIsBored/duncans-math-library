﻿using System;
using System.IO;

namespace Duncan_s_Math_Library
{
    class persistencelib
    {
        // persistence library v1.0
        // started 5/9/2019 by Duncan

        // available functions and algorithms in persistence library v1.0:
        // persistence                                              : library that performs calculations related to persistence
        //      persistence [-n number]                             : will calculate the persistence of the number
        //      persistence [-c ceil] [-pl p-level] [-s start]      : will calculate a number smaller than the ceiling number with the given persistence level
        //                                                          :   if you provide a ceiling number of 0, it will calculate the smallest number with the given persistence level
        //                                                          :   if start number provided, algorithm will start at that number instead of at 0

        // global vars
        public static string version = "1.0";

        // internal vars
        static int pNum = 0;
        static int pCeil = 0; 
        static int pLevel = 0;
        static int startNum = -1;

        // function to parse functions related to persistence library
        public static void parsePArgs(string args)
        {
            Console.WriteLine("Persistence library detected - parsing persistence arguments");
            Console.WriteLine("arguments are [" + args + "]");

            #region argument / flag parsing
            // vars for parsing string
            int index = 0; int flagCollectID = 0; char currentChar; string bufferString = null; args += " "; StringReader sr = new StringReader(args);
            // vars for determining which function to use
            bool nArg = false; bool cArg = false; bool plArg = false;

            // gathering argument data
            while (index != args.Length)
            {
                currentChar = (char)sr.Read();
                index++;
                bufferString += currentChar;
                switch (currentChar)
                {
                    case ' ':
                        if (bufferString == "-n ")          { nArg = true; flagCollectID = 1; }
                        else if (bufferString == "-c ")     { cArg = true; flagCollectID = 2; }
                        else if (bufferString == "-pl ")    { plArg = true; flagCollectID = 3; }
                        else if (bufferString == "-s ")     { flagCollectID = 4; }
                        else
                        {
                            if (flagCollectID == 1)         { pNum = Convert.ToInt32(bufferString); flagCollectID = 0; }
                            else if (flagCollectID == 2)    { pCeil = Convert.ToInt32(bufferString); flagCollectID = 0; }
                            else if (flagCollectID == 3)    { pLevel = Convert.ToInt32(bufferString); flagCollectID = 0; }
                            else if (flagCollectID == 4)    { startNum = Convert.ToInt32(bufferString); flagCollectID = 0; }
                        }
                        bufferString = null;
                        break;
                }
            }

            Console.WriteLine("Running requested persistence function ...");
            // determining function to use
            if (nArg && !cArg && !plArg) { Console.WriteLine("Persistence level is " + calculatePLevel(pNum, true)); }
            if (!nArg && cArg && pCeil != 0 && plArg) { Console.WriteLine("Number with the same persistence level smaller than ceiling is " + calculatePLevelLower(pCeil, pLevel, true)); }
            if (!nArg && cArg && pCeil == 0 && plArg) { Console.WriteLine("Smallest number with persistence level " + pLevel + " is " + calculatePLevelLower(0, pLevel, true)); }

            #endregion
        }

        public static int calculatePLevel(int input, bool outputDebug)
        {
            Console.WriteLine("Running calculatePLevel with arguments [" + input + "]");

            // internal counter for persistence level
            int pLevel = 1; // start at one just to make sure it works right

            // one-digiti test case
            if (input.ToString().Length == 1) return 0;

            TopOfAlgo:
            // break up input into digits
            string inputString = Convert.ToString(input);
            int[] digits = new int[inputString.Length];
            int index = 0; char currentChar; StringReader sr = new StringReader(Convert.ToString(inputString));
            while (index != inputString.Length)
            {
                currentChar = (char)sr.Read();
                digits[index] = Convert.ToInt16(Convert.ToString(currentChar)); index++; // have to do some weird conversion trickery to get the intended result
            }

            // dump digit array (debug only)
            if (outputDebug)
            {
                Console.WriteLine("Dumping digit array");
                string dumpDigitArrayString = "";
                for (int i = 0; i < digits.Length; i++) { dumpDigitArrayString += digits[i] + ", "; }
                Console.WriteLine("Digit array [" + dumpDigitArrayString + "]");
            }

            // multiply digits together
            int cumulativeResult = digits[0]; // start with first digit
            for (int i = 1; i < inputString.Length; i++)
            { cumulativeResult *= digits[i]; } 
            if (outputDebug) Console.WriteLine("cumulativeResult is [" + cumulativeResult + "] with len " + cumulativeResult.ToString().Length);
            if (cumulativeResult.ToString().Length > 1) { pLevel++; input = cumulativeResult; goto TopOfAlgo; }

            return pLevel;
        }

        public static int calculatePLevelLower(int ceil, int pLevel, bool debug)
        {
            Console.WriteLine("Running calculatePLevelLower with arguments [" + ceil + ", " + pLevel + "]");
            if (startNum != -1) Console.WriteLine("Starting at value " + startNum);

            // will use calculatePLevel recursively
            int num = ceil;
            if (startNum != -1) num = startNum;
            Console.WriteLine("Base pLevel is " + pLevel);

            if (ceil != 0)
            {
                num--; // start with 1 less than number that was input
                while (calculatePLevel(num, false) != pLevel) { num--; if (num == 0) { Console.WriteLine("Unable to find number smaller than ceiling with provided pLevel"); break; } }
                return num;
            }
            else
            { while (calculatePLevel(num, false) != pLevel) { num++; } return num; }
        }
    }
}

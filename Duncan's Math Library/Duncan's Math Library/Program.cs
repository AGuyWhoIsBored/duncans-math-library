﻿using System;

namespace Duncan_s_Math_Library
{
    class Program
    {
        static void Main(string[] args)
        {
            // started 5/9/2019 by Duncan
            // this class is just a simple wrapper to invoke the other libraries in this project

            // variables
            string version = "1.0";
            string userCommand;

            Console.Title = "Duncan's Math Library (wrapper) v" + version;
            Console.WriteLine("Duncan's Math Library (wrapper) v" + version);
            Console.WriteLine("This is a wrapper for some mathematics algorithms and functions within this project");

            // grab user input - syntax is [function] [arguments]
            Console.WriteLine("Please input a command: "); userCommand = Console.ReadLine();

            // parse user input
            // I should write a function for this since I use it so often
            Console.WriteLine("Beginning argument parsing ...");
            if (userCommand.Contains("persistence")) { persistencelib.parsePArgs(userCommand.Replace("persistence ", "")); } // invoke persistence library

            Console.WriteLine("Library algorithm has finished. Hit enter to terminate the user interface wrapper.");
            Console.ReadLine();
        }
    }
}
